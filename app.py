from flask import Flask, jsonify, request
from flask_cors import CORS

import psycopg2

con = psycopg2.connect(
  host = "127.0.0.1",
  database = "skripsi",
  user = "postgres",
  password = "asdf1234;"
)

app = Flask('berita-service')
CORS(app)

@app.route('/berita', methods=['GET'])
def getBerita():
  cursor = con.cursor()
  cursor.execute("SELECT * FROM berita")
  berita = cursor.fetchall()
  return jsonify(berita)

app.run(port=8000, debug=True)